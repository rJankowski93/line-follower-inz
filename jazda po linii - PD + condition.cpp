#include<QTRSensors.h>
#include <Timers.h>

#define NUM_SENSORS 6 // ilosc uzywanych czujnikow
#define NUM_SAMPLES_PER_SENSOR 4 // average 4 analog samples per sensor
#define EMITTER_PIN 2 // emitter is controlled by digital pin 2

Timers <1> timer;

QTRSensorsAnalog qtra((unsigned char[]) {
  A5, A4, A3, A2, A1, A0
},
NUM_SENSORS, NUM_SAMPLES_PER_SENSOR, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];
int sensorWeight[NUM_SENSORS] = { -40, -20, -10, 10, 20, 40};

int rightMotorForward = 3;
int rightMotorBack = 5;
int leftMotorForward = 9;
int leftMotorBack = 6;
int error = 0;
int previousError = 0;
int valuePD = 0;
int basicSpeed = 150;
int maxSpeed = 255;
int Kp = 2;
int Kd = 2;

void calculateSensor();
void calculateError();
void calculatePD();
void jazda();

void setup() {
  pinMode(rightMotorForward, OUTPUT); // piny wyjsciowe
  pinMode(rightMotorBack, OUTPUT);
  pinMode(leftMotorForward, OUTPUT);
  pinMode(leftMotorBack, OUTPUT);
  timer.attach(0, 10, jazda);
  Serial.begin(9600);
}

void loop() {
  timer.process();
}

void calculateSensor() {
  qtra.read(sensorValues);
  for (int i = 0; i < NUM_SENSORS; i++) {
    if (sensorValues[i] > 500) {
      sensorValues[i] = 1;
    } else {
      sensorValues[i] = 0;
    }
  }
}

void calculateError() {
  int counter = 0;
  error = 0;
  for (int i = 0 ; i < NUM_SENSORS ; i++) {
    error = error + (sensorValues[i] * sensorWeight[i]);
    if (sensorValues[i] == 1) {
      counter++;
    }
  }
  if (counter != 0) {
    error = error / counter;
  }
  else {
    if (previousError > -10 && previousError < 10) {
      // Serial.println("reset");
      error = 0;
    }
    else {
      error = previousError;
    }
  }
}

void calculatePD() {
  int differential = error - previousError;
  previousError = error;
  valuePD = (Kp * error) + (Kd * differential);
}

void jazda() {
  calculateSensor();
  calculateError();
  calculatePD();
  if (  valuePD < 50 && valuePD > -50) {
    analogWrite(rightMotorForward, basicSpeed + valuePD);
    analogWrite(leftMotorForward, basicSpeed - valuePD);
    analogWrite(rightMotorBack, 0);
    analogWrite(leftMotorBack, 0);
  }
  else {
    if (valuePD > 0) {
      analogWrite(rightMotorForward, maxSpeed);
	  if (valuePD < 255) {        
        analogWrite(leftMotorBack, valuePD);
        analogWrite(leftMotorForward, 0);
        analogWrite(rightMotorBack,  0);
      }
      else {
        analogWrite(leftMotorBack, maxSpeed);
        analogWrite(leftMotorForward, 0);
        analogWrite(rightMotorBack,  0);
      }
    }
    else {
	 analogWrite(leftMotorForward, maxSpeed);
     if (valuePD > -255) {
        analogWrite(rightMotorBack, -valuePD);
        analogWrite(rightMotorForward, 0);
        analogWrite(leftMotorBack,  0);
      }
      else {
        analogWrite(rightMotorBack, maxSpeed);
        analogWrite(rightMotorForward, 0);
        analogWrite(leftMotorBack,  0);
      }
    }
  }
}
